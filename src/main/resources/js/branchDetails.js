AJS.toInit(function () {
    var pageState = require("model/page-state")
    var url = AJS.contextPath() + "/rest/branchForCommit/1.0/BranchDetails" 
    	+ "/projects/" + pageState.getProject().getKey()
    	+ "/repos/" + pageState.getRepository().getSlug()
    	+ "/commits/" + pageState.getChangeset().getId() + "/value";
    AJS.$.ajax({
        type: 'GET',
        url: url,
        success: function(data) {			
       	 	AJS.$("<div class='branchDetails'></div>").insertAfter('.changeset-metadata div.changeset-metadata-changeset-id');
				function isFeatureBranch( branchName ) {
					return isNaN(branchName) && branchName !== "master";
				}
			
				// Finds numeric names and moves them to the beginning of an array
				function sortNumeric( array ) {
					var front = [];
					var end = [];
					for ( var i = 0; i < array.length; i++ ) {
						if (isFeatureBranch(array[i])) {
							end.push(array[i]);
						}
						else {
							front.push(array[i]);
						}
					}				   
				   return front.concat(end);
				}
			
               var branches = sortNumeric(data.result.split( ', ' ));
               var result = '';
               for( var i = 0; i < branches.length; i++  ) {
                   result += '<a class="' + (isFeatureBranch(branches[i]) ? 'featureBranch' : 'primaryBranch') + '" href="' +  data.baseUrl + '?until=refs/heads/' + branches[i] + '">' + branches[i] + '</a>';
                   if( i < branches.length-1 ) {
                       result += ', ';
                   }
               }
               AJS.$('.branchDetails').html( result );
        },
        error: function() {
            console.error("Error GETing branches from stash");
        },
        contentType: "application/json"
    });
});
